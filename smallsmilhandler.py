#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler (ContentHandler):
    def __init__(self):
        self.width = ""
        self.height = ""
        self.background = ""
        self.id = ""
        self.top = ""
        self.bottom = ""
        self.left = ""
        self.right = ""
        self.src = ""
        self.region = ""
        self.begin = ""
        self.dur = ""
        self.etiqueta1 = {}
        self.etiqueta2 = {}
        self.etiqueta3 = {}
        self.etiqueta4 = {}
        self.etiqueta5 = {}
        self.lista = []

    def startElement(self, name, attrs):
        if name == 'root-layout':
            self.etiqueta1 = {}
            self.etiqueta1['etiqueta'] = name
            self.width = attrs.get('width', "")
            self.etiqueta1['width'] = self.width
            self.height = attrs.get('height', "")
            self.etiqueta1['height'] = self.height
            self.background = attrs.get('background-color', "")
            self.etiqueta1['background-color'] = self.background
            self.lista.append(self.etiqueta1)

        elif name == 'region':
            self.etiqueta2 = {}
            self.etiqueta2['etiqueta'] = name
            self.id = attrs.get('id', "")
            self.etiqueta2['id'] = self.id
            self.top = attrs.get('top', "")
            self.etiqueta2['top'] = self.top
            self.bottom = attrs.get('bottom', "")
            self.etiqueta2['bottom'] = self.bottom
            self.left = attrs.get('left', "")
            self.etiqueta2['left'] = self.left
            self.right = attrs.get('right', "")
            self.etiqueta2['right'] = self.right
            self.lista.append(self.etiqueta2)

        elif name == 'img':
            self.etiqueta3 = {}
            self.etiqueta3['etiqueta'] = name
            self.src = attrs.get('src', "")
            self.etiqueta3['src'] = self.src
            self.region = attrs.get('region', "")
            self.etiqueta3['region'] = self.region
            self.begin = attrs.get('Begin', "")
            self.etiqueta3['begin'] = self.begin
            self.dur = attrs.get('dur', "")
            self.etiqueta3['dur'] = self.dur
            self.lista.append(self.etiqueta3)

        elif name == 'audio':
            self.etiqueta4 = {}
            self.etiqueta4['etiqueta'] = name
            self.src = attrs.get('src', "")
            self.etiqueta4['src'] = self.src
            self.begin = attrs.get('Begin', "")
            self.etiqueta4['begin'] = self.begin
            self.dur = attrs.get('dur', "")
            self.etiqueta4['dur'] = self.dur
            self.lista.append(self.etiqueta4)

        elif name == 'textstream':
            self.etiqueta5 = {}
            self.etiqueta5['etiqueta'] = name
            self.etiqueta5['scr'] = self.src
            self.region = attrs.get('region', "")
            self.etiqueta5['region'] = self.region
            self.lista.append(self.etiqueta5)

    def get_tags(self):
        return self.lista


if __name__ == "__main__":
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))
    print(cHandler.get_tags())
