#!/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from smallsmilhandler import SmallSMILHandler
import sys
import json
import urllib.request


class KaraokeLocal(SmallSMILHandler):
    def __init__(self, fichero):
        # Declaro variables y leo fichero del shell
        parser = make_parser()
        cHandler = SmallSMILHandler()
        parser.setContentHandler(cHandler)
        parser.parse(open(fichero))
        self.lista = cHandler.get_tags()

    def __str__(self):
        self.resultado = ''
        # El primer for es para leer cada elemento de la lista
        for etiqueta in self.lista:
            # Para leer cada atributo dentro del elemento de la lista
            for atribut in etiqueta:
                if etiqueta[atribut] != "":
                    self.resultado += atribut+"="+etiqueta[atribut]+'\t'
            self.resultado += '\n'
        return(self.resultado)

    def to_json(self, fichero):
        # .replace cambia la extension de .smil a .json
        self.nuevofichero = fichero.replace('.smil', '.json')
        # abrir el nuevo fichero y en formato escritura para en la siguiente
        # linea poder modificarlo y que se adapte a la extension json
        with open(self.nuevofichero, 'w') as ficherojson:
            json.dump(self.lista, ficherojson, indent=4)

    def do_local(self, fichero):
        for etiqueta in self.lista:
            # Para leer cada atributo dentro del elemento de la lista
            for atribut in etiqueta:
                # Dentro del atributo src
                # me quedo con los valores que empiecen por http:
                if atribut == 'src'and etiqueta[atribut].startswith('http'):
                    urllib.request.urlretrieve(etiqueta[atribut],
                                               etiqueta[atribut].split('/')[-1]
                                               )


if __name__ == "__main__":
    try:
        fichero = sys.argv[1]
        karaoke = KaraokeLocal(fichero)
        print(karaoke)
        karaoke.to_json(fichero)
        karaoke.do_local(fichero)
        karaoke.to_json('local.json')
        print(karaoke)

    except IndexError:
        sys.exit("Usage: python3 karaoke.py file.smil")
